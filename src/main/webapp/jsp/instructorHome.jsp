<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ taglib tagdir="/WEB-INF/tags/instructor" prefix="ti" %>
<%@ taglib tagdir="/WEB-INF/tags/instructor/home" prefix="home" %>
<c:set var="jsIncludes">
    <script type="text/javascript" src="/js/instructor.js"></script>
    <script type="text/javascript" src="/js/instructorHome.js"></script>
    <script type="text/javascript" src="/js/ajaxResponseRate.js"></script>
    <script type="text/javascript" src="/js/instructorFeedbackAjaxRemindModal.js"></script>
</c:set>
<ti:instructorPage pageTitle="TEAMMATES - Instructor" bodyTitle="Instructor Home" jsIncludes="${jsIncludes}">
    <home:search />
    <br />
    <t:statusMessage />
    <ti:remindParticularStudentsModal />
    

    

        
    <%-- --%>
    <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Example of Bootstrap 3 Modals</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>
</head>
<body>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Getting Started</h4>
            </div>
            <div class="modal-body">
                <p>
                	Before to start, you can follow the next basic instructions as Instructor:
                </p>
                
                <ol>
                	<li>
                
                        <span class="text-bold">Create a course
                        </span>
                        <div class="helpSectionContent">
                            Go to the ‘Courses’ page and create a course.
                            <br>Some of the elements in the user interface (e.g., text boxes) have hover over tips to tell you what the element does.
                            <br>
                        </div>
                        <br>
                        <div id="gettingStartedHtml" class="bs-example">
                            <div class="panel panel-primary">
                                <div class="panel-body fill-plain">
                                    <form class="form form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Course ID:
                                            </label>
                                            <div class="col-sm-3">
                                                <input data-original-title="Enter the identifier of the course, e.g.CS3215-2013Semester1." class="form-control" value="" data-toggle="tooltip" data-placement="top" maxlength="40" tabindex="1" placeholder="e.g. CS3215-2013Semester1" title="" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Course Name:
                                            </label>
                                            <div class="col-sm-9">
                                                <input data-original-title="Enter the name of the course, e.g. Software Engineering." class="form-control" value="" data-toggle="tooltip" data-placement="top" maxlength="64" tabindex="2" placeholder="e.g. Software Engineering" title="" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <input class="btn btn-primary" value="Add Course" tabindex="3" type="button">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br>
                        
                    </li>
                    
                	<li>
                
                
                
                
                
                
                        <span class="text-bold">Enroll students
                        </span>
                        <div class="helpSectionContent">
                            Enroll students by clicking the ‘Enroll’ link for the relevant course. That link is available in the ‘Courses’ page and the ‘Home’ page.
                            <br>
                            <br>
                        </div>
                    
                
                	</li>
                	
                	<li>
                        <span class="text-bold">Create a session
                        </span>
                        <div class="helpSectionContent">
                            Go to the ‘Sessions’ page and create a session (there are different session types to choose from).
                            <br>
                            <br>

                            <div class="bs-example" id="sessionTypeSelectionHtml">
                                <div class="well well-plain">
                                    <div title="" class="row" data-original-title="Select a different type of session here." data-toggle="tooltip" data-placement="top">
                                        <h4 class="label-control col-md-2 text-md">Create new </h4>
                                        <div class="col-md-5">
                                            <select name="fstype" class="form-control" id="fstype">
                                                <option selected="selected" value="STANDARD">
                                                    Session with your own questions
                                                </option>
                                                <option value="TEAMEVALUATION">
                                                    Team peer evaluation session
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                             
                        </div>
                    </li>
                    
                    <li>
                    <span class="text-bold">If you need more instructions for getting started, please Click in</span>
                    <br>
                    <br>
                    <a class='btn btn-primary btn-lg btn-block' href="/instructorHelp.html" target="_blank">Getting Started</a>
                    
                    <button type="button" class="btn btn-success btn-lg btn-block" data-dismiss="modal">Close</button>
                	</li>
                	
                	
<!--                 	<button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button> -->
                    
               
               	
               

                </ol>
              
            </div>
            
            <!--
             
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
             
            -->
            
            
        </div>
    </div>
</div>
</body>
</html>                                		
    <%-- --%>
    

    
    <c:if test="${data.account.instructor}">
        <home:sort isSortButtonsDisabled="${data.sortingDisabled}"/>
        <br />
        <c:forEach items="${data.courseTables}" var="courseTable" varStatus="i">
            <home:coursePanel courseTable="${courseTable}" index="${i.index}" />
        </c:forEach>
        <ti:copyModal />
    </c:if>
</ti:instructorPage>